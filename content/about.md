---
title: "About"
date: "2019-07-19"
---

This website is the landing page of PyChemApps and gives an entry point to
the applications available on this server. The server is hosted by the
[University of Pay & Pays Adour](https://www.univ-pau.fr).

_PyChemApps_, is a server dedicated to serve applications written in python about
chemical physics. The applications mainly rely on two technologies which are
[Django](https://www.djangoproject.com/) and [Plotly/Dash](https://plot.ly/dash/).

For more informations, please, contact [Germain Salvato Vallverdu](https://gsalvatovallverdu.gitlab.io/).
