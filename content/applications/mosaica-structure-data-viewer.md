---
author: "Germain Salvato Vallverdu"
date: 2019-07-19
title: Structural data viewer
featured_image: "/img/mosaica.png"
---

This [Plotly/Dash](https://plot.ly/dash/) application aims to visualize local
geometric informations about a
molecular structure. In particular, the application computes geometric
quantities which provide an insight of the _discrete curvature_ of a molecular
structure. In addition, from upload or by editing the table, you can visualize
any atomic properties.

## Application

The application is available at
[pychemapps.univ-pau.fr/mosaica/](https://pychemapps.univ-pau.fr/mosaica/).

## Screenshots

![mosaica Screenshot](/img/mosaica.png)
