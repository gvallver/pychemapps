---
author: "Germain Salvato Vallverdu"
date: 2019-06-15
title: Imaging LA-ICP MS experiments
featured_image: "/img/la-icpms-2.png"
---

This is a [Plotly/Dash](https://plot.ly/dash/) application which aims to help
in the analyzes of Inductively Coupled Plasma Mass Spectrometry (ICPMS)
experiments used to perform a chemical mapping from laser ablation.

## Application

The application is available at
[pychemapps.univ-pau.fr/icpms/](https://pychemapps.univ-pau.fr/icpms/).

## Screenshots

### Upload tab
![Upload tab](/img/la-icpms-1.png)

### Chemical mapping analyzes

![Imaging tab](/img/la-icpms-2.png)

### Data exploration and statistical analyzes

![Data exploration tab](/img/la-icpms-3.png)
